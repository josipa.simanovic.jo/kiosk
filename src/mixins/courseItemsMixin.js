export default {
  data() {
    return {
      courseItems: [
        {
          id: 1,
          courseImage: `../../src/assets/img/course/ZPMladost1.jpg`,
          listImg: `../src/assets/img/course/list/course_list_1.jpeg`,
          lesson: "43",
          title: "Zimsko plivalište mladost",
          sports: [
            {
              id: "s1",
              sport: "Plivanje",
              sportImage: `../../src/assets/img/icon/sport/swimm.svg`,
            },
            {
              id: "s2",
              sport: "Vaterpolo",
              sportImage: `../../src/assets/img/icon/sport/waterpolo.svg`,
            },
            {
              id: "s3",
              sport: "Skokovi",
              sportImage: `../../src/assets/img/icon/sport/jump.svg`,
            },
          ],
          objekti: [
            {
              id: "o1",
              objektName: "Odrasli",
              objektImage: `fa-user`,
            },
            {
              id: "o2",
              objektName: "Djeca",
              objektImage: `fa-child`,
            },
            {
              id: "o3",
              objektName: "Umirovljenici",
              objektImage: `fa-blind`,
            },
            {
              id: "o4",
              objektName: "Škole",
              objektImage: `fa-school`,
            },
            {
              id: "o4",
              objektName: "Poduka",
              objektImage: `fa-users-class`,
            },
            {
              id: "o5",
              objektName: "Paketi",
              objektImage: `fa-box-open`,
            },
          ],
          rating: "4.5",
          teacherImg: `../src/assets/img/course/teacher/teacher-1.jpg`,
          teacherName: "Jim Séchen",
          category: "Art & Design",
          price: "21.00",
          oldPrice: "33.00",
        },
        {
          id: 2,
          courseImage: `../src/assets/img/course/sp-Mladost.jpg`,
          listImg: `../src/assets/img/course/list/course_list_2.jpeg`,
          lesson: "72",
          title: "Sportski part Mladost",
          rating: "4.0",
          teacherImg: `../src/assets/img/course/teacher/teacher-2.jpg`,
          teacherName: "Barry Tone",
          category: "UX Design",
          price: "32.00",
          oldPrice: "68.00",
          color: "sky-blue",
        },
        {
          id: 3,
          courseImage: `../src/assets/img/course/resize.jfif`,
          listImg: `../src/assets/img/course/list/course_list_5.jpeg`,
          lesson: "28",
          title: "SRC Šalata",
          rating: "4.3",
          teacherImg: `../src/assets/img/course/teacher/teacher-3.jpg`,
          teacherName: "Samuel Serif",
          category: "Development",
          price: "13.00",
          oldPrice: "19.00",
          color: "green",
        },
        {
          id: 4,
          courseImage: `../src/assets/img/course/bksvetice.jpg`,
          listImg: `../src/assets/img/course/list/course_list_4.jpeg`,
          lesson: "60",
          title: "Bazenski kompleks Svetice",
          rating: "3.5",
          teacherImg: `../src/assets/img/course/teacher/teacher-4.jpg`,
          teacherName: "Elon Gated",
          category: "Development",
          price: "62.00",
          oldPrice: "97.00",
          color: "blue",
        },
        {
          id: 5,
          courseImage: `../src/assets/img/course/Bazen-Iver-thumb.jpg`,
          listImg: `../src/assets/img/course/list/course_list_5.jpeg`,
          lesson: "28",
          title: "Bazen Iver",
          rating: "4.5",
          teacherImg: `../src/assets/img/course/teacher/teacher-5.jpg`,
          teacherName: "Eleanor Fant",
          category: "Marketing",
          price: "25.00",
          oldPrice: "36.00",
          color: "orange",
        },
        {
          id: 6,
          courseImage: `../src/assets/img/course/bazen_jelkovec1.jpg`,
          listImg: `../src/assets/img/course/list/course_list_6.jpeg`,
          lesson: "38",
          title: "Bazen Jelkovec",
          rating: "4.8",
          teacherImg: `../src/assets/img/course/teacher/teacher-6.jpg`,
          teacherName: "Brian Cumin",
          category: "Data Science",
          price: "35.00",
          oldPrice: "46.00",
          color: "pink",
        },
        {
          id: 7,
          courseImage: `../src/assets/img/course/Početna-Bazen-Utrina-02.jpg`,
          listImg: `../src/assets/img/course/list/course_list_7.jpeg`,
          lesson: "26",
          title: "Bazenski komplekst Utrina",
          rating: "4.6",
          teacherImg: `../src/assets/img/course/teacher/teacher-7.jpg`,
          teacherName: "Pelican Steve",
          category: "Audio & Music",
          price: "46.00",
          oldPrice: "72.00",
          color: "orange",
        },
        {
          id: 8,
          courseImage: `../src/assets/img/course/course-8.jpg`,
          listImg: `../src/assets/img/course/list/course_list_8.jpeg`,
          lesson: "13",
          title: "Creative writing through Storytelling",
          rating: "4.4",
          teacherImg: `../src/assets/img/course/teacher/teacher-8.jpg`,
          teacherName: "Shahnewaz Sakil",
          category: "Mechanical",
          price: "52.00",
          oldPrice: "72.00",
          color: "pink",
        },
        {
          id: 9,
          courseImage: `../src/assets/img/course/course-9.jpg`,
          listImg: `../src/assets/img/course/list/course_list_9.jpeg`,
          lesson: "25",
          title: "Product Manager Learn the Skills & job.",
          rating: "4.2",
          teacherImg: `../src/assets/img/course/teacher/teacher-9.jpg`,
          teacherName: "Hilary Ouse",
          category: "Lifestyle",
          price: "15.00",
          oldPrice: "45.00",
          color: "blue-2",
        },
        {
          id: 10,
          courseImage: `../src/assets/img/course/course-9.jpg`,
          listImg: `../src/assets/img/course/list/course_list_9.jpeg`,
          lesson: "25",
          title: "Product Manager Learn the Skills & job.",
          rating: "4.2",
          teacherImg: `../src/assets/img/course/teacher/teacher-9.jpg`,
          teacherName: "Hilary Ouse",
          category: "Lifestyle",
          price: "15.00",
          oldPrice: "45.00",
          color: "blue-2",
        },
        {
          id: 11,
          courseImage: `../src/assets/img/course/course-9.jpg`,
          listImg: `../src/assets/img/course/list/course_list_9.jpeg`,
          lesson: "25",
          title: "Product Manager Learn the Skills & job.",
          rating: "4.2",
          teacherImg: `../src/assets/img/course/teacher/teacher-9.jpg`,
          teacherName: "Hilary Ouse",
          category: "Lifestyle",
          price: "15.00",
          oldPrice: "45.00",
          color: "blue-2",
        },
      ],
    };
  },
};
